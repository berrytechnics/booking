var deviceTree = [
  {
    text:'Phone & Tablet',
    selectable:false,
    state:{expanded:false},
    nodes: [
      {
        text:'Apple',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {
            text:'iPhone',
            selectable:false,
            state:{expanded:false},
            nodes:[
              {text:'iPhone 5'},
              {text:'iPhone 5C'},
              {text:'iPhone 5S'},
              {text:'iPhone SE'},
              {text:'iPhone 6'},
              {text:'iPhone 6+'},
              {text:'iPhone 6s'},
              {text:'iPhone 6s+'},
              {text:'iPhone 7'},
              {text:'iPhone 7+'},
              {text:'iPhone 8'},
              {text:'iPhone 8+'},
              {text:'iPhone X'},
              {text:'iPhone Xr'},
              {text:'iPhone Xs'},
              {text:'iPhone Xs Max'}
            ]
          },
          {
            text:'iPad',
            selectable:false,
            state:{expanded:false},
            nodes:[
              {text:'iPad 2'},
              {text:'iPad 3'},
              {text:'iPad 4'},
              {text:'iPad Air 1'},
              {text:'iPad Air 2'},
              {text:'iPad (2017)'},
              {text:'iPad Mini 1'},
              {text:'iPad Mini 2'},
              {text:'iPad Mini 3'},
              {text:'iPad Mini 4'},
              {text:'iPad Pro 10.5"'},
              {text:'iPad Pro 12.9"'},
              {text:'iPad (2018)'},
            ],
          }
        ]
      },//END Apple
      {
        text:'Samsung',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {
            text:'Galaxy S',
            selectable:false,
            state:{expanded:false},
            nodes:[
              {text:'Galaxy S4'},
              {text:'Galaxy S5'},
              {text:'Galaxy S6'},
              {text:'Galaxy S6 Edge'},
              {text:'Galaxy S6 Edge+'},
              {text:'Galaxy S6 Active'},
              {text:'Galaxy S7'},
              {text:'Galaxy S7 Edge'},
              {text:'Galaxy S7 Active'},
              {text:'Galaxy S8'},
              {text:'Galaxy S8+'},
              {text:'Galaxy S8 Active'},
              {text:'Galaxy S9'},
              {text:'Galaxy S9+'},
                ]
              },
            {
              text:'Galaxy J',
              selectable:false,
              state:{expanded:false},
              nodes:[
                {text:'Galaxy J3'},
                {text:'Galaxy J7'},
              ]
            },
            {
              text:'Galaxy Note',
              selectable:false,
              state:{expanded:false},
              nodes:[
                {text:'Note 2'},
                {text:'Note 3'},
                {text:'Note 4'},
                {text:'Note 5'},
                {text:'Note Edge'},
                {text:'Note 8'},
                {text:'Note 9'},
              ]
            },
        ],
      },//END Samsung
      {
        text:'Motorola',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'Moto X (1st gen)'},
          {text:'Moto X (2nd gen)'},
          {text:'Moto X4'},
          {text:'Moto G (1st gen)'},
          {text:'Moto G (2nd gen)'},
          {text:'Droid Turbo'},
          {text:'Droid Turbo 2'},
          {text:'Droid Maxx'},
        ]
      },//END Motorola
      {
        text:'LG',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'V30'},
          {text:'V20'},
          {text:'VU 3'},
          {text:'Optimus VU 2'},
          {text:'G6'},
          {text:'G5'},
          {text:'G4'},
          {text:'G Flex 2'},
          {text:'G Flex'},
          {text:'G3'},
          {text:'G2'}
        ],
      },//END lG
      {
        text:'HTC',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'One A9'},
          {text:'One M9'},
          {text:'One M8'},
          {text:'One M7'},
        ]
      },//END HTC
      {
        text:'Google',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'Pixel 2 XL'},
          {text:'Pixel 2'},
          {text:'Pixel XL'},
          {text:'Pixel'},
          {text:'Nexus 6P'},
          {text:'Nexus 6'},
        ]
      },//END Google
    ]
  },
  {
    text:'Computer',
    selectable:false,
    state:{expanded:false},
    nodes:[
      {text:'Windows'},
      {
        text:'Mac',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'Mac Mini'},
          {text:'iMac'},
          {
            text:'Macbook',
            selectable:false,
            state:{expanded:false},
            nodes:[
              {text:'Macbook Unibody (Late 2009-2011)'},
              {text:'Macbook (2006-2009)'},
              {text:'Macbook (2015- )'}
            ]
          },
          {
            text:'Macbook Air',
            selectable:false,
            state:{expanded:false},
            nodes:[
              {text:'Macbook air (2008-2010)'},
              {text:'Macbook Air Unibody (2010- )'}
            ]
          },
          {
            text:'Macbook Pro',
            selectable:false,
            state:{expanded:false},
            nodes:[
              {text:'Macbook Pro Aluminum (2006-2008)'},
              {text:'Macbook Pro Unibody (2009- Mid 2012)'},
              {text:'Macbook Pro Retina (Late 2012-2015)'}
            ]
          }
        ]
      },
    ]
  },
  {
    text:'Console',
    selectable:false,
    state:{expanded:false},
    nodes:[
      {
        text:'PlayStation',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'PS3'},
          {text:'PS4'},
          {text:'PS4 Slim'},
          {text:'PS4 Pro'},
          {text:'PSP'}
        ]
      },
      {
        text:'xBox',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'360'},
          {text:'One'},
          {text:'One X'}
        ]
      },
      {
        text:'Nintendo',
        selectable:false,
        state:{expanded:false},
        nodes:[
          {text:'DS'},
          {text:'DS Lite'},
          {text:'DS XL'},
          {text:'3DS'},
          {text:'Wii'},
          {text:'Wii U'},
          {text:'Switch'}
        ]
      }
    ]
  },
  {text:'Other'}
];

var phoneRepairTree = [
  {text:'Screen Repair'},
  {text:'Charge Port'},
  {text:'Battery Replacement'},
  {text:'Other Repair'}
]
var tabletRepairTree = [
  {text:'Digitizer Replacement'},
  {text:'LCD Replacement'},
  {text:'Charge Port'},
  {text:'Battery Replacement'},
  {text:'Other Repair'}
];
var computerRepairTree = [
  {text:'LCD Repair'},
  {text:'Battery Replacement'},
  {text:'Software & Virus Removal'},
  {text:'Other'}
];
var consoleRepairTree = [
  {text:'HDMI Port'},
  {text:'Disc Drive'},
  {text:'Other'},
]

var phonePriceTable = {
  //-----------iPhone
  "iPhone 5":{
    'Screen Repair':'$49.99',
    'Charge Port':'$29.99',
    'Battery Replacement':'$19.99',
    'Other Repair':'Varies'
  },
  "iPhone 5C":{
    'Screen Repair':'$49.99',
    'Charge Port':'$29.99',
    'Battery Replacement':'$19.99',
    'Other Repair':'Varies'
  },
  "iPhone 5S":{
    'Screen Repair':'$49.99',
    'Charge Port':'$29.99',
    'Battery Replacement':'$19.99',
    'Other Repair':'Varies'
  },
  "iPhone SE":{
    'Screen Repair':'$59.99',
    'Charge Port':'$39.99',
    'Battery Replacement':'$29.99',
    'Other Repair':'Varies'
  },
  "iPhone 6":{
    'Screen Repair':'$59.99',
    'Charge Port':'$39.99',
    'Battery Replacement':'$39.99',
    'Other Repair':'Varies'
  },
  "iPhone 6+":{
    'Screen Repair':'$69.99',
    'Charge Port':'$39.99',
    'Battery Replacement':'$49.99',
    'Other Repair':'Varies'
  },
  "iPhone 6s":{
    'Screen Repair':'$69.99',
    'Charge Port':'$49.99',
    'Battery Replacement':'$49.99',
    'Other Repair':'Varies'
  },
  'iPhone 6s+':{
    'Screen Repair':'$79.99',
    'Charge Port':'$49.99',
    'Battery Replacement':'$59.99',
    'Other Repair':'Varies'
  },
  'iPhone 7':{
    'Screen Repair':'$89.99',
    'Charge Port':'$59.99',
    'Battery Replacement':'$59.99',
    'Other Repair':'Varies'
  },
  'iPhone 7+':{
    'Screen Repair':'$99.99',
    'Charge Port':'$59.99',
    'Battery Replacement':'$69.99',
    'Other Repair':'Varies'
  },
  'iPhone 8':{
    'Screen Repair':'$99.99',
    'Charge Port':'$59.99',
    'Battery Replacement':'$69.99',
    'Other Repair':'Varies'
  },
  'iPhone 8+':{
    'Screen Repair':'$119.99',
    'Charge Port':'$69.99',
    'Battery Replacement':'$89.99',
    'Other Repair':'Varies'
  },
  'iPhone X':{
    'Screen Repair':'$199.99',
    'Charge Port':'$99.99',
    'Battery Replacement':'$99.99',
    'Other Repair':'Varies'
  },
  'iPhone Xr':{
    'Screen Repair':'TBD',
    'Charge Port':'TBD',
    'Battery Replacement':'TBD',
    'Other Repair':'Varies'
  },
  'iPhone Xs':{
    'Screen Repair':'TBD',
    'Charge Port':'TBD',
    'Battery Replacement':'TBD',
    'Other Repair':'Varies'
  },
  'iPhone Xs Max':{
    'Screen Repair':'TBD',
    'Charge Port':'TBD',
    'Battery Replacement':'TBD',
    'Other Repair':'Varies'
  },
  //-------iPad
  'iPad 2':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad 3':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad 4':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Air 1':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Air 2':{
    'Screen Repair':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad (2017)':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Mini':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Mini 2':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Mini 3':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Mini 4':{
    'Screen Repair':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Pro 10.5"':{
    'Screen Repair':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad Pro 12.9"':{
    'Screen Repair':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  },
  'iPad (2018)':{
    'Digitizer Replacement':'',
    'LCD Replacement':'',
    'Charge Port':'',
    'Battery Replacement':'',
    'Other':'Varies'
  }
  //---END APPLE

}
var computerPriceTable = {};
var consolePriceTable = {};

$(document).ready(()=>{
  $('#deviceTree').treeview({data: deviceTree});
  $('#phoneRepairTree').treeview({data:phoneRepairTree});
  $('#tabletRepairTree').treeview({data:tabletRepairTree});
  $('#computerRepairTree').treeview({data:computerRepairTree});
  $('#consoleRepairTree').treeview({data:consoleRepairTree});

  $('#deviceTree').on('nodeSelected', function(event, data) {
    // console.log(data.parentId)
    $('#device').val(data.text)
    if(data.parentId=='19'){
      $('#deviceTree').fadeOut(500);
      setTimeout(()=>{$('#tabletRepairTree').fadeIn(500)},500)
    }
    else if(data.parentId>1&&data.parentId<87){
      $('#deviceTree').fadeOut(500);
      setTimeout(()=>{$('#phoneRepairTree').fadeIn(500)},500)
    }
    else if(data.parentId>92&&data.parentId<106){
      $('#deviceTree').fadeOut(500);
      setTimeout(()=>{$('#computerRepairTree').fadeIn(500)},500)
    }
    else if(data.parentId>109&&data.parentId<121){
      $('#deviceTree').fadeOut(500);
      setTimeout(()=>{$('#consoleRepairTree').fadeIn(500)},500)
    }
    else if(!data.parentId){
      $('#deviceTree').fadeOut(500);
      setTimeout(()=>{$('#otherDiv').fadeIn(500)},500)
      $('#price').val('Varies')
    }
  });

  $('#phoneRepairTree').on('nodeSelected', function(event, data) {
    $('#repairType').val(data.text)
    $('#price').val(phonePriceTable[$('#device').val()][$('#repairType').val()])
  });
  $('#computerRepairTree').on('nodeSelected', function(event, data) {
    $('#repairType').val(data.text)
    $('#price').val(computerPriceTable[$('#device').val()][$('#repairType').val()])
  });
  $('#consoleRepairTree').on('nodeSelected', function(event, data) {
    $('#repairType').val(data.text)
    $('#price').val(consolePriceTable[$('#device').val()][$('#repairType').val()])
  });
})

$('#deviceSearch').keyup(()=>{
  $('#deviceTree').treeview($('#deviceSearch').val(), [ 'Parent', {
  ignoreCase: true,     // case insensitive
  exactMatch: false,    // like or equals
  revealResults: true,  // reveal matching nodes
}])});

function nextStep(){
  $('.contactSection').hide();
  $('.repairSection').show();
};

function restart(){
  $('#phoneRepairTree').hide();
  $('#tabletRepairTree').hide();
  $('#computerRepairTree').hide();
  $('#consoleRepairTree').hide();
  $('#otherDiv').hide();
  $('#deviceTree').show();
  $('#deviceTree').treeview('clearSearch'); // NOT WORKING
};
